const routes = [
  {
    path: "/",
    component: () => import("layouts/Layout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      {
        path: "/info",
        name: "InfoDeFramework",
        component: () => import("pages/Info.vue"),
        props: true
      },
      {
        path: "/400",
        name: "Error400",
        component: () => import("pages/Error400.vue"),
        props: true
      },
      {
        path: "/loading",
        name: "PantallaDeCarga",
        component: () => import("pages/Loading.vue"),
        props: true
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
